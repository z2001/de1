package geometries;

import java.util.List;
import java.util.Map;

import primitives.Color;
import primitives.Point3D;
import primitives.Ray;
import primitives.Vector;

public class Cylinder extends RadialGeometry{
	
	Point3D _axisPoint;
	Vector _axisDirection;
	
	// ***************** Constructors ********************** // 
	public Cylinder(double _radius, Point3D _axisPoint, Vector _axisDirection, Color _emission) {
		super(_radius,_emission);
		this._axisPoint = _axisPoint;
		this._axisDirection = _axisDirection;
	}
	
	// ***************** Getters/Setters ********************** //
	public Point3D get_axisPoint() {
		return _axisPoint;
	}
	public Vector get_axisDirection() {
		return _axisDirection;
	}
	
	// ***************** Operations ******************** // 
	@Override
	public Vector getNormal (Point3D p) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Map<Geometry,List<Point3D>> findIntersections(Ray r) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Color getEmission() {
		// TODO Auto-generated method stub
		return null;
	}
}
