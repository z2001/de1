package geometries;

import primitives.Color;
import primitives.Point3D;
import primitives.Ray;
import primitives.Vector;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import static primitives.Util.*;

public class Plane extends Geometry {

	Point3D _p;
	Vector _normal;
	// ***************** Constructors ********************** //
	/**
	 * build plane from three point3D
	 * 
	 * @param p1 first point3D
	 * @param p2 second point3D
	 * @param p3 third point3D
	 */
	public Plane(Point3D p1, Point3D p2, Point3D p3,Color emission) {
		this._emission = emission;
		Vector v1 = p2.sub(p1);
		Vector v2 = p3.sub(p2);
		_normal = v1.crossProduct(v2).normalize();
		_p = new Point3D(p1);
	}

	/**
	 * build plane from point3D and a vector and then normalize the vector
	 * 
	 * @param p point3D
	 * @param v normalized vector
	 */
	public Plane(Point3D p, Vector v) {
		this._normal = v.normalize();
		this._p = new Point3D(p);
	}

	// ***************** Getters/Setters ********************** //
	public Point3D getP() {
		return _p;
	}

	public Vector getNormal() {
		return _normal;
	}

	// ***************** Operations ******************** //
	//protected static final List<Point3D> EMPTY = new ArrayList<Point3D>();

	/**
	 * find intersections between plane and ray and return the intersection point in
	 * a list that contains point3D or an empty list there are no intersections
	 * between the plane and the ray
	 */
	@Override
	public  Map<Geometry,List<Point3D>> findIntersections(Ray r) {
		Point3D p0 = r.getP00();
		Vector v = r.getDirection();

		double denom = this._normal.dotProduct(v);
		if (isZero(denom)) // ray is parallel to the plane (or included in it)
			return EMPTY;

		if (p0.equals(_p)) { // ray begins at the plane
			List<Point3D> list = new ArrayList<Point3D>();
			Map<Geometry,List<Point3D>> map = new HashMap<Geometry,List<Point3D>>();//
			list.add(_p);
			map.put(this,list);
			return map;
		}

		double t = _normal.dotProduct(p0.sub(_p)) / -denom;
		if (isZero(t)) { // ray start from the plane
			List<Point3D> list = new ArrayList<Point3D>();
			Map<Geometry,List<Point3D>> map = new HashMap<Geometry,List<Point3D>>();//
			list.add(p0);
			map.put(this,list);
			return map;
		}
		if (t > 0) { // it is not behind the ray
			List<Point3D> list = new ArrayList<Point3D>();
			Map<Geometry,List<Point3D>> map = new HashMap<Geometry,List<Point3D>>();//
			list.add(p0.add(v.scale(t)));
			map.put(this,list);
			return map;
		}

		return EMPTY;
	}

	/**
	 * @return _normal
	 */
	@Override
	public Vector getNormal(Point3D p) {
		return _normal;
	}

	@Override
	public Color getEmission() {
		return _emission;
	}
}
