package geometries;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import primitives.Color;
import primitives.Point3D;
import primitives.Ray;
import primitives.Vector;
import static primitives.Util.*;

public class Sphere extends RadialGeometry {

	Point3D _center;
	// ***************** Constructors ********************** //
	/**
	 * Build a sphere from a center point and a radius
	 * @param _radius radius of the sphere
	 * @param _center center point of the sphere
	 */
	public Sphere(double _radius, Point3D _center, Color emission) {
		super(_radius,emission);
		this._center = _center;
	}
	// ***************** Getters/Setters ********************** //

	public Point3D get_center() {
		return _center;
	}

	// ***************** Operations ******************** //
	/**
	 * create a vector from the center outwards and normalize it
	 * 
	 * @param p point3D on the sphere
	 */
	@Override
	public Vector getNormal(Point3D p) {
		return p.sub(_center).normalize();
	}
	/**
	 * find intersections between sphere and ray and return the intersection points
	 * in a list that contains point3D or an empty list if there are no intersections
	 * between the sphere and the ray
	 */
	@Override
	public Map<Geometry,List<Point3D>> findIntersections(Ray r) {
		Point3D p0 = r.getP00();
		Vector v = r.getDirection();
		Vector u;
		try {
			u = _center.sub(p0);//if p0=center
		} catch (Exception e) {
			ArrayList<Point3D> list = new ArrayList<Point3D>();
			Map<Geometry,List<Point3D>> map = new HashMap<Geometry,List<Point3D>>();//
			list.add(p0.add(v.scale(_radius)));
			map.put(this,list);
			return map;
		}

		double tm = v.dotProduct(u);
		double d2 = u.length2() - tm * tm;
		double d = Math.sqrt(d2);
		if (d > this._radius)
			return EMPTY;
		
		Map<Geometry,List<Point3D>> map = new HashMap<Geometry,List<Point3D>>();//
		ArrayList<Point3D> list = new ArrayList<Point3D>();
		double th = Math.sqrt(this._radius2 - d2);
		if (isZero(th)) {
			if (tm >= 0)
				list.add(p0.add(v.scale(tm)));
				map.put(this,list);
			return map;
		}

		double t = tm - th;
		if (t > 0)
			list.add(p0.add(v.scale(t)));
		t = tm + th;
		if (t > 0)
			list.add(p0.add(v.scale(t)));
		map.put(this,list);
		return map;
	}

	@Override
	public Color getEmission() {
		return _emission;
	}
}
