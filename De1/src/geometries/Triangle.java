package geometries;

import primitives.Color;
import primitives.Point3D;
import primitives.Ray;
import primitives.Vector;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class Triangle extends Plane {
	Point3D _p1;
	Point3D _p2;
	Point3D _p3;

	// ***************** Constructors ********************** //
	/**
	 * build triangle from three point3D
	 * 
	 * @param _p1 first point3D
	 * @param _p2 second point3D
	 * @param _p3 third point3D
	 */
	public Triangle(Point3D _p1, Point3D _p2, Point3D _p3, Color emission) {
		// The plane constructor will not allow consolidated points
		// or all points on the same line - and it is good for us
		super(_p1, _p2, _p3, emission);
		this._p1 = _p1;
		this._p2 = _p2;
		this._p3 = _p3;
		this._emission = emission;
	}

	// ***************** Getters ********************** //
	public Point3D get_p1() {
		return _p1;
	}

	public Point3D get_p2() {
		return _p2;
	}

	public Point3D get_p3() {
		return _p3;
	}

	// ***************** Operations ******************** //
	/**
	 * find intersections between triangle and ray and return the intersection point
	 * in a list that contains point3D or an empty list there are no intersections
	 * between the triangle and the ray
	 */
	@Override
	public Map<Geometry, List<Point3D>> findIntersections(Ray r) {
		Map<Geometry, List<Point3D>> map = super.findIntersections(r);
		if (map.isEmpty())
			return EMPTY;

		Point3D p = map.get(this).get(0);
		// Point3D p = map.get(r.g);
		Point3D p0 = r.getP00();
		if (p.equals(p0))
			p0 = p0.add(r.getDirection().scale(-10));

		Vector n1, n2, n3;
		double a, b, c;
		try {
			Vector v1 = _p1.sub(p0);
			Vector v2 = _p2.sub(p0);
			Vector v3 = _p3.sub(p0);
			n1 = v1.crossProduct(v2);
			n2 = v2.crossProduct(v3);
			n3 = v3.crossProduct(v1);
		} catch (Exception e) {
			return EMPTY;
		}

		a = p.sub(p0).dotProduct(n1);
		b = p.sub(p0).dotProduct(n2);
		c = p.sub(p0).dotProduct(n3);

		if (a > 0 && b > 0 && c > 0 || a < 0 && b < 0 && c < 0)
			return map;

		return EMPTY;
	}
}
