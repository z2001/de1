package geometries;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import primitives.Point3D;
import primitives.Ray;

public class Geometries implements Intersectable, Iterable<Intersectable> {
	private List<Intersectable> _geometries;

	public Geometries() {
		_geometries = new ArrayList<Intersectable>();
	}

	public void add(Intersectable a) {
		this._geometries.add(a);
	}

	@Override
	public Map<Geometry, List<Point3D>> findIntersections(Ray r) {
		Map<Geometry, List<Point3D>> intersections = new HashMap<Geometry, List<Point3D>>();
		for (Intersectable g : this._geometries) {
			if (!g.findIntersections(r).isEmpty())
				intersections.putAll(g.findIntersections(r));
		}
		return intersections;
	}

	public Iterator<Intersectable> iterator() {
		return _geometries.iterator();
	}
}
