package geometries;
import java.util.List;
import java.util.Map;

import primitives.*;

public interface Intersectable {
	public Map<Geometry, List<Point3D>> findIntersections (Ray r);
}
