package geometries;

import static primitives.Util.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import primitives.Color;
import primitives.Point3D;

public abstract class RadialGeometry extends Geometry {
	protected double _radius;
	protected double _radius2;

	// ***************** Constructors ********************** //
	/**
	 * build a geometry that contains radius and color
	 * 
	 * @param radius   radius of geometry
	 * @param emission color of geometry
	 */
	public RadialGeometry(double radius, Color emission) {
		if (isZero(radius))
			throw new IllegalArgumentException("Zero radius is not allowed");
		this._radius = radius;
		_radius2 = radius * radius;
		this._emission = emission;
	}

	// ***************** Administration ******************** //
	public double getRadius() {
		return _radius;
	}

	public double getRadius2() {
		return _radius2;
	}

}
