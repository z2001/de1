package geometries;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import primitives.*;

public abstract class Geometry implements Intersectable {
	protected static final Map<Geometry, List<Point3D>> EMPTY = new HashMap<Geometry,List<Point3D>>();
	protected Color _emission;


	// ***************** Constructors ********************** //
	public Geometry() {
		
	}
	
	public Geometry(Color em) {
		_emission = new Color(em);
	}

	// ***************** Operations ******************** //
	// getNormal
	abstract public Vector getNormal(Point3D p);

	abstract public Color getEmission();
}
