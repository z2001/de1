package unittests;

import org.junit.Test;

import renderer.ImageWriter;

class ImageWriterTest {

	@Test
	public void ImageWriter() {
		ImageWriter a = new ImageWriter("Test", 500, 500, 50, 50);
		for (int i = 0; i < 500; i++) {
			for (int j = 0; j < 500; j++) {
				if (i % 50 == 0 || j % 50 == 0)
					a.writePixel(i, j, 255, 255, 255);
			}
		}
		a.writeToimage();
	}

}