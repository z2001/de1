package unittests;

import org.junit.Test;

import elements.AmbientLight;
import elements.Camera;
import geometries.*;
import primitives.*;
import renderer.ImageWriter;
import renderer.Render;
import scene.Scene;

public class RenderTest {
	@Test
	public void basicRendering() {
		Scene scene = new Scene("Test scene");
		scene.setCamera(new Camera(new Point3D(0, 0, 0), new Vector(0, -1, 0), new Vector(0, 0, 1)));
		scene.setDistance(100);
		scene.setBackground(new Color(0, 0, 0));
		Geometries geometries = new Geometries();
		scene.setGeometries(geometries);
		AmbientLight ambientLight = new AmbientLight(new Color(0, 0, 0), 1);
		scene.setAmbientLight(ambientLight);
		
		geometries.add(new Sphere(67, new Point3D(0, 0, 150), new Color(51,51,51)));
		
		geometries.add(new Triangle(new Point3D(150, 0, 149), new Point3D(0, 150, 149), new Point3D(150, 150, 149),new Color(251,102,101)));//lb
		
		geometries.add(new Triangle(new Point3D(150, 0, 149), new Point3D(0, -150, 149), new Point3D(150, -150, 149),new Color(101,251,101)));//lt

		geometries.add(new Triangle(new Point3D(-150, 0, 149), new Point3D(0, 150, 149), new Point3D(-150, 150, 149),new Color(101,101,251)));//rb

		geometries.add(new Triangle(new Point3D(-150, 0, 149), new Point3D(0, -150, 149), new Point3D(-150, -150, 149),new Color(51,51,51)));//rt
	
		ImageWriter imageWriter = new ImageWriter("test0", 500, 500, 500, 500);
		Render render = new Render(imageWriter, scene);
		
		render.renderImage();
		render.printGrid(50);
		render.writeToImage();
		
	}
}