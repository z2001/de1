package unittests;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

import geometries.Geometry;
import geometries.Triangle;
import primitives.Point3D;
import primitives.Ray;
import primitives.Vector;

class TriangleTest {

	@Test

	public void testNormal() {

	}

	@Test
	public void testIntersectionPoints() {
		ArrayList<Point3D> l = new ArrayList<Point3D>();
		Map<Geometry, List<Point3D>> map = new HashMap<Geometry,List<Point3D>>();
		Point3D p1 = new Point3D(11, 0, -2);
		Point3D p2 = new Point3D(0, 0, 6);
		Point3D p3 = new Point3D(-4, 0, -2);
		Triangle tr = new Triangle(p1, p2, p3,null);
		Point3D t;
		// parallel not included in the triangle
		Vector direction = new Vector(-2, 0, 0);
		Point3D p0 = new Point3D(2, 3, 0);
		Ray r = new Ray(p0, direction);
		assertEquals(map, tr.findIntersections(r));
		// parallel included in the plane
		direction = new Vector(-2, 0, 0);
		p0 = new Point3D(3, 0, 0);
		r = new Ray(p0, direction);
		assertEquals(map, tr.findIntersections(r));
		// Ray is orthogonal to the plane -in
		direction = new Vector(0, 3, 0);
		p0 = new Point3D(2, 0, 0);
		r = new Ray(p0, direction);
		t = new Point3D(2, 0, 0);
		l.add(t);
		map.put(tr, l);
		assertEquals(map, tr.findIntersections(r));
		l.clear();
		map.clear();
		// Ray is orthogonal to the plane -before -
		direction = new Vector(0, 2, 0);
		p0 = new Point3D(2, -4, 0);
		r = new Ray(p0, direction);
		t = new Point3D(2, 0, 0);
		l.add(t);
		map.put(tr, l);
		assertEquals(map, tr.findIntersections(r));
		l.clear();
		map.clear();
		// Ray is orthogonal to the plane -after
		direction = new Vector(0, 3, 0);
		p0 = new Point3D(3, 3, 0);
		r = new Ray(p0, direction);
		assertEquals(map, tr.findIntersections(r));
		// Ray begins at the plane
		direction = new Vector(2, 3, 0);
		p0 = new Point3D(1, 0, 0);
		r = new Ray(p0, direction);
		l.add(p0);
		map.put(tr, l);
		assertEquals(map, tr.findIntersections(r));
		l.clear();
		map.clear();
		// random intersection
		direction = new Vector(-2, -1, 0);
		p0 = new Point3D(4, 3, 0);
		r = new Ray(p0, direction);
		t = new Point3D(-2, 0, 0);
		l.add(t);
		map.put(tr, l);
		assertEquals(map, tr.findIntersections(r));
		l.clear();
		map.clear();
		// random no intersection
		direction = new Vector(-2, -3, 0);
		p0 = new Point3D(-4, 3, 0);
		r = new Ray(p0, direction);
		assertEquals(map, tr.findIntersections(r));
	}

}
