package unittests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import elements.Camera;
import primitives.Point3D;
import primitives.Ray;
import primitives.Vector;

class CameraTest {

	@Test
	public void testRaysConstruction() {
		Point3D p0 = Point3D.ZERO;
		Camera c = new Camera(p0, new Vector(0, 0, -14), new Vector(0, -20, 0));
		//Camera c = new Camera(p0, new Vector(0, -20, 0), new Vector(0, 0, -14));
		int Nx = 3;
		int Ny = 3;
		int i = 0;
		int j = 0;
		double screenDistance = 1;
		double screenWidth = 3;
		double screenHeight = 3;
		Vector v = new Vector (1,-1,-2);
		Ray r1 = new Ray(p0, v);
		// (0,0)
		Ray r = new Ray(c.constructRayThroughPixel(Nx, Ny, i, j, screenDistance, screenWidth, screenHeight));
		Ray t = new Ray(p0, new Vector(-6, 6, 1));
		assertEquals(t, r);
		// (3,3)
		i = 3;
		j = 3;
		r = new Ray(c.constructRayThroughPixel(Nx, Ny, i, j, screenDistance, screenWidth, screenHeight));
		t = new Ray(p0, new Vector(-6, 6, 1));
		assertEquals(t.toString(), r.toString());
		// (-3,-3)
		i = -3;
		j = -3;
		r = new Ray(c.constructRayThroughPixel(Nx, Ny, i, j, screenDistance, screenWidth, screenHeight));
		t = new Ray(p0, new Vector(-6, 6, 1));
		assertEquals(t.toString(), r.toString());
		// (4.5,4.5)
		// i=4.5;
		// j=4.5
		r = new Ray(c.constructRayThroughPixel(Nx, Ny, i, j, screenDistance, screenWidth, screenHeight));
		t = new Ray(p0, new Vector(-6, 6, 1));
		assertEquals(t.toString(), r.toString());
	
		c = new Camera(p0, new Vector(0, 2, 0), new Vector(0, 0, 1));
		Nx = 150;
		Ny = 150;
		i = 30;
		j = 30;
		screenDistance = 100;
		screenWidth = 150;
		screenHeight = 150;

		r = new Ray(c.constructRayThroughPixel(Nx, Ny, i, j, screenDistance, screenWidth, screenHeight));
		t = new Ray(p0, new Vector(-91, 200, 45.5));
		assertEquals(t.toString(), r.toString());//*/

	}

}
