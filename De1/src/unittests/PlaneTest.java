package unittests;

import static org.junit.Assert.assertEquals;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

import geometries.Geometry;
import geometries.Plane;
import primitives.Point3D;
import primitives.Ray;
import primitives.Vector;

class PlaneTest {

	@Test

	public void testNormal() {
		Vector v = new Vector(3.5, -5, 10);
		v = v.normalize();
		assertEquals("", 1, v.length(), 1e-10);

		try {
			v = new Vector(0, 0, 0);
			v.normalize();
		} catch (Exception e) {
			assertTrue(true);
		}
	}

	@Test
	public void testIntersectionPoints() {
		// plane
		Point3D p1 = new Point3D(0, 0, 1);
		Point3D p2 = new Point3D(1, 0, 0);
		Point3D p3 = new Point3D(-3, 0, 0);
		Plane p = new Plane(p1, p2, p3,null);
		ArrayList<Point3D> l = new ArrayList<Point3D>();
		Map<Geometry, List<Point3D>> map = new HashMap<Geometry,List<Point3D>>();
		Point3D t;
		// parallel not included in the plane - pass
		Vector direction = new Vector(-2, 0, 0);
		Point3D p0 = new Point3D(2, 3, 0);
		Ray r = new Ray(p0, direction);
		assertEquals(map, p.findIntersections(r));
		// parallel included in the plane -pass
		direction = new Vector(-2, 0, 0);
		p0 = new Point3D(2, 0, 0);
		r = new Ray(p0, direction);
		assertEquals(map, p.findIntersections(r));
		// Ray is orthogonal to the plane -in -pass
		direction = new Vector(0, 3, 0);
		p0 = new Point3D(2, 0, 0);
		r = new Ray(p0, direction);
		t = new Point3D(2, 0, 0);
		l.add(t);
		map.put(p, l);
		assertEquals(map, p.findIntersections(r));
		l.clear();
		map.clear();
		// Ray is orthogonal to the plane -before -
		direction = new Vector(0, 2, 0);
		p0 = new Point3D(2, -4, 0);
		r = new Ray(p0, direction);
		t = new Point3D(2, 0, 0);
		l.add(t);
		map.put(p, l);
		assertEquals(map, p.findIntersections(r));// error
		l.clear();
		map.clear();
		// Ray is orthogonal to the plane -after
		direction = new Vector(0, 3, 0);
		p0 = new Point3D(3, 3, 0);
		r = new Ray(p0, direction);
		assertEquals(map, p.findIntersections(r));
		// Ray begins at the plane
		direction = new Vector(2, 5, 0);
		p0 = new Point3D(1, 0, 0);
		r = new Ray(p0, direction);
		l.add(p0);
		map.put(p, l);
		assertEquals(map, p.findIntersections(r));
		l.clear();
		map.clear();
		// random intersection
		direction = new Vector(1, -2, 0);
		p0 = new Point3D(7, 4, 0);
		r = new Ray(p0, direction);
		t = new Point3D(9, 0, 0);
		l.add(t);
		map.put(p, l);
		assertEquals(map, p.findIntersections(r));
		l.clear();
		map.clear();
		// random no intersection
		direction = new Vector(-4, 2, 0);
		p0 = new Point3D(0, 2, 0);
		r = new Ray(p0, direction);
		assertEquals(map, p.findIntersections(r));
	}

}
