package unittests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import primitives.Point3D;
import primitives.Vector;

class VectorTests {

	@Test
	public void addvectorTest() {

		Vector v1 = new Vector(1, 0, 0);
		Vector v2 = new Vector(-7, 0, 0);
		Vector v3 = new Vector(-6, 0, 0);
		assertEquals(v3, v1.add(v2));

		// same direction
		v1 = new Vector(1, 0, 0);
		v2 = new Vector(7, 0, 0);
		v3 = new Vector(8, 0, 0);
		assertEquals(v3, v1.add(v2));

		// orth
		v1 = new Vector(1, 0, 0);
		v2 = new Vector(0, 1, 0);
		v3 = new Vector(1, 1, 0);
		assertEquals(v3, v1.add(v2));

		// regular
		v1 = new Vector(3, -3, 0);
		v2 = new Vector(4, 2, 0);
		v3 = new Vector(7, -1, 0);
		assertEquals(v3, v1.add(v2));
	}

	@Test
	public void subVectorTests() {

		Vector v1 = new Vector(3, -3, 0);
		Vector v2 = new Vector(2, -2, 0);
		Vector v3 = new Vector(-1, 1, 0);
		assertEquals(v3, v2.sub(v1));

		// same direction vectors
		v1 = new Vector(0, -1, -1);
		v2 = new Vector(0, -2, -2);
		v3 = new Vector(0, -1, -1);
		assertEquals(v3, v2.sub(v1));

		// Orthogonal vectors
		v1 = new Vector(0, 0, 1);
		v2 = new Vector(0, 1, 0);
		v3 = new Vector(0, 1, -1);
		assertEquals(v3, v2.sub(v1));

		// regular vectors
		v1 = new Vector(0, 0, 1);
		v2 = new Vector(2, 1, 5);
		v3 = new Vector(2, 1, 4);
		assertEquals(v3, v2.sub(v1));

	}

	@Test
	public void dotproducttest() {

		// Opposite vectors
		Vector v1 = new Vector(1, 2, 3);
		Vector v2 = new Vector(-1, -2, -3);
		double dotproductResult = -14;
		assertEquals(dotproductResult, v1.dotProduct(v2));

		// Same direction vectors
		v1 = new Vector(1, 2, 3);
		v2 = new Vector(2, 4, 6);
		dotproductResult = 28;
		assertEquals(dotproductResult, v1.dotProduct(v2));

		// Orthogonal vectors
		v1 = new Vector(1, 2, 3);
		v2 = new Vector(0, 3, -2);
		dotproductResult = 0;
		assertEquals(dotproductResult, v1.dotProduct(v2));

		// regular vectors
		v1 = new Vector(1, 2, 3);
		v2 = new Vector(1, 3, 4);
		dotproductResult = 19;
		assertEquals(dotproductResult, v1.dotProduct(v2));
	}

	@Test
	public void Crossproducttest() {
		// BV tests
		// 1. Opposite vectors
		// code...
		//
		// 2. Same direction vectors
		// 3. Orthogonal vectors

		// EP test - non co-directed or opp-directed or orth vectors
/*
		// Opposite vectors
		Vector v1 = new Vector(5, 6, 7);
		Vector v2 = new Vector(-5, -6, -7);
		Vector v3 = new Vector(0, 0, 0);
		try {
			assertEquals(v3, v1.crossProduct(v2));
			fail("The vector(0,0,0 cannot be defined");
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		}

		// Same direction vectors
		v1 = new Vector(5, 6, 7);
		v2 = new Vector(10, 12, 14);
		try {
			// v3 = new Vector(0,0,0);
			assertEquals(v3, v1.crossProduct(v2));
			fail("The vector(0,0,0 cannot be defined");
		} catch (IllegalArgumentException e) {
			assertTrue(true);
		}*/

		// Orthogonal vectors
		Vector v1 = new Vector(1, 0, 0);
		Vector v2 = new Vector(0, 1, 0);
		Vector v3 = new Vector(0, 0, 1);
		assertEquals(v3, v1.crossProduct(v2));

		// regular vectors
		v1 = new Vector(5, 6, 7);
		v2 = new Vector(8, 9, 3);
		v3 = new Vector(-45, 41, -3);
		assertEquals(v3, v1.crossProduct(v2));

	}

	@Test
	public void lengthtest() {
		Vector v1 = new Vector(2, 4, -2);
		assertEquals(Math.sqrt(24), v1.length(), 0.000001, "Wrong vector length");
	}

	@Test
	public void scale() {
		Vector v1 = new Vector(1, 2, 3);
		double t = 4;
		Vector v2 = new Vector(4, 8, 12);
		assertEquals(v2, v1.scale(t));
	}

	@Test
	public void normal() {
		Vector v1 = new Vector(0, 4, 3);
		Vector v2 = new Vector(0, 0.8, 0.6);
		assertEquals(v2, v1.normalize());
	}
}
