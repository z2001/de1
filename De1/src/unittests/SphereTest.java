package unittests;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

import geometries.Geometry;
import geometries.Sphere;
import primitives.Point3D;
import primitives.Ray;
import primitives.Vector;

class SphereTest {

	@Test
	public void testNormal() {
		Sphere s= new Sphere(1, new Point3D(0,0,0),null);
		Point3D p=new Point3D(3, 0, 0);
		assertEquals(new Vector(1,0,0),s.getNormal(p));
		try {
			p = new Point3D(0, 0, 0);
			s.getNormal(p);
		} catch (Exception e) {
			assertTrue(true);
		}
	}
	@Test
	public void testIntersectionPoints() {
		ArrayList<Point3D> l = new ArrayList<Point3D>();
		double radius = 3;
		Point3D center = new Point3D(0, 0, 0);
		Sphere s = new Sphere(radius, center,null);
		Map<Geometry, List<Point3D>> map = new HashMap<Geometry,List<Point3D>>();
		//����
		Point3D p0 = new Point3D(2, -1, 0);
		Vector direction = new Vector(-0.52, 0.52, 0);	
		Ray r = new Ray(p0, direction);
		l.add(new Point3D(0, 1, 0));
		l.add(new Point3D(1, 0, 0));
		assertEquals(l, s.findIntersections(r));
		//p0=center
		p0 = new Point3D(0, 0, 0);
		direction = new Vector(0, 4, 0);	
		r = new Ray(p0, direction);
		l.add(new Point3D(0, 3, 0));
		assertEquals(l, s.findIntersections(r));
		//
		radius = 3;
		direction = new Vector(1.43, -0.45, 0);
		p0 = new Point3D(0, 4, 0);
		r = new Ray(p0, direction);
		center = new Point3D(0, 0, 0);
		s = new Sphere(radius, center,null);
		assertEquals(l, s.findIntersections(r));
		// - ����� ���� �����(�� ���� ���� �����)
		direction = new Vector(1.72, 3.13, 0);
		p0 = new Point3D(1.76, 3.06, 0);
		r = new Ray(p0, direction);
		assertEquals(l, s.findIntersections(r));
		// ���� ����� ��� ��� ����� (���� �����)
		direction = new Vector(2.73, 2.87, 2.86);
		p0 = new Point3D(0.64, 0.63, -2.86);
		r = new Ray(p0, direction);
		assertEquals(l, s.findIntersections(r));
		// ���� ����� ����� �����
		direction = new Vector(1, 0, 0);
		p0 = new Point3D(0, 0, 0);
		r = new Ray(p0, direction);
		assertEquals(l, s.findIntersections(r));
		// ����� ����� ���� ��� ���� �����
		direction = new Vector(0, 1, 0);
		p0 = new Point3D(0, 3, 0);
		r = new Ray(p0, direction);
		assertEquals(l, s.findIntersections(r));
		// ����� ���� ��� �� ����� �� ����� ������ ���� ���� �����
		direction = new Vector(1, 0, 0);
		p0 = new Point3D(4, 0, 0);
		r = new Ray(p0, direction);
		assertEquals(l, s.findIntersections(r));
		// ������ ���� ����� �� ��������� �� ������ �� 2 ������ �����
		direction = new Vector(0, 1, 0);
		p0 = new Point3D(3.38, 0, 0);
		r = new Ray(p0, direction);
		assertEquals(l, s.findIntersections(r));
		// ������ ���� ����� ������ �� ����� ������ (�� 2 ������ �����)
		direction = new Vector(-1.77, 1.73, 1.01);
		p0 = new Point3D(4, 0, 0);
		r = new Ray(p0, direction);
		assertEquals(l, s.findIntersections(r));
		// ���� �����
		direction = new Vector(-3.37, 4.85, -0.99);
		p0 = new Point3D(2.45, 1.41, 0.99);
		r = new Ray(p0, direction);
		assertEquals(l, s.findIntersections(r));
		// ����� ����� ��� ������ ��� ����� ����� �������
		direction = new Vector(7, -7, 0);
		p0 = new Point3D(7, -7, 0);
		r = new Ray(p0, direction);
		assertEquals(l, s.findIntersections(r));
		// ����� ����� �� ������ ������� ������
		
	}
}
