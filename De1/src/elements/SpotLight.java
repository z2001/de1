package elements;

import primitives.Color;
import primitives.Point3D;
import primitives.Vector;

public class SpotLight extends PointLight {
	
	private Vector direction;
	private Vector position;
	
	public SpotLight(Color c, Point3D _position, double _kc, double _kq, double _kl, double d, Vector direction) {
		super(c, _position, _kc, _kq, _kl);
		this.direction = direction.normalize();
	}
	
	@Override
	public Color getIntensity(Point3D point) {
		return super.getIntensity(point).scale(direction.dotProduct(super.getL(point)));
	}
}
