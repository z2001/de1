package elements;

import primitives.Vector;
import primitives.Point3D;
import primitives.Ray;

public class Camera {
	// components
	Point3D _p0;
	Vector _vUp;
	Vector _vTo;
	Vector _vRight;

	// ***************** Constructors ********************** //
	/**
	 * Construct a camera from point3D the represent the center of projection an
	 * upwards vector and a vector towards the plane of view and normalize them then
	 * creates a vector to the right from the cross product of the two vectors and
	 * normalize the vector
	 * 
	 * @param _p0  center of projection
	 * @param _vUp upwards vector
	 * @param _vTo from the camera towards the plane of view
	 */
	public Camera(Point3D _p0, Vector _vUp, Vector _vTo) {
		if (_vTo.dotProduct(_vUp) != 0)
			throw new IllegalArgumentException("vectors are not orthogonal");
		this._p0 = _p0;
		this._vUp = _vUp.normalize();
		this._vTo = _vTo.normalize();

	}

	// ***************** Getters/Setters ********************** //
	public Point3D get_p0() {
		return _p0;
	}

	public Vector get_vUp() {
		return _vUp;
	}

	public Vector get_vTo() {
		return _vTo;
	}

	public Vector get_vRight() {
		return _vRight;
	}

	// ***************** Operations ******************** //
	/**
	 * TEST Construct ray through pixel for a certain (i,j) pixel
	 * 
	 * @param Nx             number of pixels on the x axis
	 * @param Ny             number of pixels on the y axis
	 * @param i              x value of pixel
	 * @param j              y value of pixel
	 * @param screenDistance distance of the screen from the camera
	 * @param screenWidth    width of the screen
	 * @param screenHeight   height of the screen
	 * @return
	 */
	public Ray constructRayThroughPixel(int Nx, int Ny, int i, int j, double screenDistance, double screenWidth,
			double screenHeight) {
		Point3D Pc = this._p0.add(this._vTo.scale(screenDistance));
		double Ry = screenHeight / Ny;
		double Rx = screenWidth / Nx;
		double Yj = (j - Ny / 2) * Ry - Ry / 2;
		double Xi = (i - Nx / 2) * Rx - Rx / 2;
		Point3D p = Pc;
		if (Xi != 0)
			p = p.add(_vRight.scale(Xi));
		if (Yj != 0)
			p = p.add(_vUp.scale(-Yj));
		Ray r = new Ray(_p0, p.sub(_p0).normalize());
		return r;
	}
}

//Ray r = new Ray(_p0, (Pc.add(_vRight.scale(Xi).sub(_vUp.scale(Yj)))).sub(_p0).normalize());