package elements;

import primitives.Color;

public abstract class Light {
	private Color _color;

	public Light(Color c) {
		_color = new Color(c);
	}

	public Color getIntensity() {
		return _color;
	}
}
