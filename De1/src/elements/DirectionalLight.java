package elements;

import primitives.Color;
import primitives.Point3D;
import primitives.Vector;

public class DirectionalLight extends Light implements LightSource {
	private Vector _direction;

	public DirectionalLight(Color c, Vector dir) {
		super(c);
		_direction = dir.normalize();
	}

	@Override
	public Color getIntensity(Point3D point) {
		return getIntensity();
	}

	@Override
	public Vector getL(Point3D point) {
		return _direction;
	}

	@Override
	public Vector getD(Point3D point) {
		return _direction;
	}



}