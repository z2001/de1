package elements;

import primitives.Color;

import primitives.Point3D;
import primitives.Vector;

public class PointLight extends Light implements LightSource {
	protected Point3D _position;
	protected double _kc, _kq, _kl;

	// ************************constructor*****************/

	public PointLight(Color c, Point3D _position, double _kc, double _kq, double _kl) {
		super(c);
		this._position = _position;
		this._kc = _kc;
		this._kq = _kq;
		this._kl = _kl;
	}

	/**
	 * return color after calculating it
	 */

	@Override
	public Vector getL(Point3D point) {
		return point.sub(_position).normalize();
	}

	@Override
	public Vector getD(Point3D point) {
		return getL(point);
	}

	@Override
	public Color getIntensity(Point3D point) {
		double d2 = _position.distance2(point);
		double d = Math.sqrt(d2);
		double sum = _kc + _kl * d + _kq * d2;
		return getIntensity().reduce(sum);
	}
}
