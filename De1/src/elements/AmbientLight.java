package elements;

import primitives.Color;

public class AmbientLight extends Light {
	private double ka;

	public AmbientLight(Color color, double ka) {
		super(color.scale(ka));
		this.ka = ka;
	}

	// **************************getters*************************************
	public double getKa() {
		return ka;
	}
}
