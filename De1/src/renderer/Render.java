package renderer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import geometries.Geometry;
import primitives.Color;
import primitives.Point3D;
import primitives.Ray;
import scene.Scene;

public class Render {
	Scene _scene;
	ImageWriter _imageWriter;

	/**
	 * build a renderer from imageWriter and scene
	 * 
	 * @param imageWriter
	 * @param scene       scene of the render
	 */
	public Render(ImageWriter imageWriter, Scene scene) {
		this._imageWriter = imageWriter;
		this._scene = scene;
	}

	/**
	 * a class that contains geometry and a point
	 */
	private static class GeoPoint {
		public Geometry geometry;
		public Point3D point;
	}

	/**
	 * render image using the intersection points of the geometries in the scene
	 */
	public void renderImage() {
		for (int i = 0; i < this._imageWriter.getNx(); i++) {
			for (int j = 0; j < this._imageWriter.getNy(); j++) {
				Ray ray = this._scene.getCamera().constructRayThroughPixel(this._imageWriter.getNx(),
						this._imageWriter.getNy(), i, j, this._scene.getDistance(), this._imageWriter.getWidth(),
						this._imageWriter.getHeight());
				Map<Geometry, List<Point3D>> intersectionPoints = this._scene.getGeometries().findIntersections(ray);
				if (intersectionPoints.isEmpty())
					_imageWriter.writePixel(i, j, this._scene.getBackground().getColor());
				else {
					GeoPoint closestPoint = getClosestPoint(intersectionPoints);
					_imageWriter.writePixel(i, j, this.calcColor(closestPoint).getColor());
				}
			}
		}
	}

	/**
	 * printing a line for every a pixels in the x and y axis
	 * 
	 * @param a pixel number
	 */
	public void printGrid(int a) {
		for (int p = 0; p < _imageWriter.getNx(); p++) {
			for (int q = 0; q < _imageWriter.getNy(); q++) {
				if (p % a == 0 || q % a == 0)
					_imageWriter.writePixel(p, q, 255, 255, 255);
			}
		}
	}

	/**
	 * writes to image
	 */
	public void writeToImage() {
		_imageWriter.writeToimage();

	}

	/**
	 * finds the closest intersection point to a point
	 * 
	 * @param intersectionPoints
	 * @return closest point
	 */
	private GeoPoint getClosestPoint(Map<Geometry, List<Point3D>> intersectionPoints) {
		double distance = Double.MAX_VALUE;
		Point3D P0 = this._scene.getCamera().get_p0();
		GeoPoint geopoint = new GeoPoint();
		for (Map.Entry<Geometry, List<Point3D>> entry : intersectionPoints.entrySet()) {
			for (Point3D point : entry.getValue())
				if (P0.distance(point) < distance) {
					geopoint.point = point;
					geopoint.geometry = entry.getKey();
					distance = P0.distance(point);
				}
		}
		return geopoint;
	}
	
	/**
	 * add ambient light and emmision of a geometry
	 * 
	 * @param geopoint
	 * @return ambient light + emission
	 */
	private Color calcColor(GeoPoint geopoint) {
		Color color = new Color(_scene.getAmbientLight().getIntensity());
		color = color.add(geopoint.geometry.getEmission());
		return color;
	}

}
