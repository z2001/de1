
package scene;

import primitives.Color;
import java.util.Iterator;
import java.util.List;

import elements.AmbientLight;
import elements.Camera;
import geometries.Geometries;
import geometries.Geometry;
import geometries.Intersectable;
import primitives.Point3D;
import primitives.Ray;

public class Scene {
	private String _name;
	private Color _background;
	private AmbientLight _ambientLight;
	private Geometries _geometries;
	private Camera _camera;
	private double _distance;

	// ***************************setters**********************

	public void setAmbientLight(AmbientLight ambientLight) {
		_ambientLight=ambientLight;
		
	}
	/***
	 *  Creates a scene and sets a name
	 * @param name scene name
	 */
	public Scene(String name) {
		_name = name;
	}

	public void setScene(String name) {
		_name = name;
	}

	public void setCamera(Camera camera) {
		_camera = camera;
	}

	public void setGeometries(Geometries geometries) {
		_geometries = geometries;
	}

	public void setDistance(double distance) {
		_distance = distance;
	}

	public void setBackground(Color color) {
		_background = color;
	}

	// *****************************getters***********************
	public Camera getCamera() {
		return _camera;
	}

	public Geometries getGeometries() {
		return _geometries;
	}

	public double getDistance() {
		return _distance;
	}

	public Color getBackground() {
		return _background;
	}

	public AmbientLight getAmbientLight() {
		return _ambientLight;
	}
}

