package primitives;


public class Point2D {
	protected Coordinate _x;
	protected Coordinate _y;

	// ***************** Constructors ********************** //
	/**
	 *  build point 2D from two double
	 * 
	 * @param _x the left coordinate
	 * @param _y the right coordinate
	 */
	public Point2D(double _x, double _y) {
		this._x = new Coordinate(_x);
		this._y = new Coordinate(_y);
	}

	/**
	 * build point 2D from two coordinate
	 * 
	 * @param _x the left coordinate
	 * @param _y the right coordinate
	 */
	public Point2D(Coordinate _x, Coordinate _y) {
		this._x = new Coordinate(_x);
		this._y = new Coordinate(_y);
	}

	/**
	 * Coordinate copy constructor
	 * 
	 * @param other Coordinate to copy from
	 */
	public Point2D(Point2D other) {
		this._x = new Coordinate(other._x);
		this._y = new Coordinate(other._y);
	}

	// ***************** Getters/Setters ********************** //
	public Coordinate getX() {
		return _x;
	}

	public Coordinate getY() {
		return _y;
	}

	// ***************** Administration ******************** //
	@Override
	/**@param obj
	 * the function checks two of point 2D and check if they equals
	 * @return true if equal false if not
	 */
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Point2D))
			return false;
		Point2D other = (Point2D) obj;
		return (this._x.equals(other.getX()) && this._y.equals(other.getY()));
	}

	/**
	 * the function return the components of the point2D
	 */
	public String toString() {
		return "(" + _x + "," + _y + ")";
	}

}
