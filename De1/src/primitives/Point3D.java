package primitives;

public class Point3D extends Point2D {
	private Coordinate _z;
	public static Point3D ZERO = new Point3D(0, 0, 0);

	// ***************** Constructors ********************** //
	/**
	 * 
	 * @param _x
	 * @param _y
	 * @param _z
	 */
	public Point3D(double _x, double _y, double _z) {
		super(_x, _y);
		this._z = new Coordinate(_z);
	}

	/**
	 * build point 3D from three coordinate
	 * 
	 * @param _x the left coordinate
	 * @param _y the right coordinate
	 * @param _z the z value
	 */
	public Point3D(Coordinate _x, Coordinate _y, Coordinate _z) {
		super(_x, _y);
		this._z = new Coordinate(_z);
	}

	/**
	 * Coordinate copy constructor
	 * 
	 * @param other Coordinate to copy from
	 */
	public Point3D(Point3D other) {
		super(other);
		_z = new Coordinate(other._z);
	}

	// ***************** Getters/Setters ********************** //

	public Coordinate getZ() {
		return _z;
	}

	// ***************** Administration ******************** //
	/**
	 * the function checks two of point 3D and check if they equals
	 * 
	 * @param obj
	 * @return true if equal false if not
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Point3D))
			return false;
		Point3D other = (Point3D) obj;
		return (super.equals(other) && _z.equals(other._z));
	}

	/**
	 * the function return the components of the point3D
	 */
	public String toString() {
		return "(" + _x + "," + _y + "," + _z + ")";
	}

	// ***************** Operations ******************** //
	/**
	 * Subtract one point3D from the other
	 * 
	 * @param other point3D
	 * @return Vectors for the subtraction of two point3D
	 */
	public Vector sub(Point3D other) {
		return new Vector(_x.subtract(other._x), _y.subtract(other._y), _z.subtract(other._z));
	}

	/**
	 * Add Point3D to Vector
	 * 
	 * @param vector
	 * @return Point3D which is the sum of Point3D and a vector
	 */
	public Point3D add(Vector other) {
		return new Point3D(this._x.add(other.getHead()._x), this._y.add(other.getHead()._y),
				this._z.add(other.getHead()._z));
	}

	/**
	 * Multiply point3D and a number
	 * 
	 * @param number
	 * @return point3D multiplied by a number
	 */
	public Point3D scale(double num) {
		return new Point3D(_x.scale(num), _y.scale(num), _z.scale(num));
	}

	/**
	 * the function calculate the distance between two points
	 * 
	 * @param other the other point that we calc the distance with
	 * @return the distance between two points
	 */
	public double distance2(Point3D other) {
		double x = _x.subtract(other._x);
		double y = _y.subtract(other._y);
		double z = _z.subtract(other._z);
		return x * x + y * y + z * z;
	}

	/**
	 * the function send to the func distance2 to calc the distance between 2 points
	 * 
	 * @param other including the other point that we calc the distance with
	 * @return the distance between between the two points
	 */
	public double distance(Point3D other) {
		return Math.sqrt(distance2(other));
	}

}
