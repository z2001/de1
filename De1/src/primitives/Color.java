package primitives;

/**
 * 
 * @author �����
 *
 */
public class Color {

	public static final Color BLACK = new Color(java.awt.Color.BLACK);
	private double _r, _g, _b;

	// ***********************Constructors***************

	/**
	 * build a color from 3 double
	 * 
	 * @param r= red color
	 * @param g= green color
	 * @param b= blue color
	 */
	public Color(double r, double g, double b) {
		this._r = r;
		this._g = g;
		this._b = b;
	}

	/**
	 * build a color from other color
	 * 
	 * @param r= red color
	 * @param g= green color
	 * @param b= blue color
	 */
	public Color(Color _color) {
		this._r = _color._r;
		this._g = _color._g;
		this._b = _color._b;
	}

	/**
	 * build a color from other color
	 * 
	 * @param r= red color
	 * @param g= green color
	 * @param b= blue color
	 */
	public Color(java.awt.Color color) {
		this._r = color.getRed();
		this._g = color.getGreen();
		this._b = color.getBlue();
	}

/////////////////////////////////////////////////////////////////////////////

//**************************getter**************************
	public java.awt.Color getColor() {
		return new java.awt.Color(_r > 255 ? 255 : (int) _r, _g > 255 ? 255 : (int) _g, _b > 255 ? 255 : (int) _b);
	}

	// *********************setters***************************
	public void setColor(java.awt.Color color) {
		this._r = color.getRed();
		this._g = color.getGreen();
		this._b = color.getBlue();
	}

	public void setColor(Color _color) {
		this._r = _color._r;
		this._g = _color._g;
		this._b = _color._b;
	}

	public void setColor(double _r, double _g, double _b) {
		this._r = _r;
		this._g = _g;
		this._b = _b;
	}

	//////////////////////////////////////////////////////////////////////////////////
	/**
	 * multiplies the RGB values in color in the factor
	 * 
	 * @param factor
	 * @return RGB values of color * factor
	 */
	public Color scale(double factor) {
		if (factor < 0 || factor > 1)
			throw new IllegalArgumentException("Bad scale factor");
		return new Color(_r * factor, _g * factor, _b * factor);
	}

	/**
	 * divides the RGB values in color in the factor
	 * 
	 * @param factor
	 * @return RGB values of color / factor
	 */
	public Color reduce(double factor) {
		if (factor < 1)
			throw new IllegalArgumentException("Bad reduce factor");
		return new Color(_r / factor, _g / factor, _b / factor);
	}

	/**
	 * add RGB values of colors
	 * 
	 * @param colors
	 * @return sum of RGB values of colors
	 */
	public Color add(Color... colors) {
		double r = _r;
		double g = _g;
		double b = _b;
		for (Color c : colors) {
			r += c._r;
			g += c._g;
			b += c._b;
		}
		return new Color(r, g, b);
	}
}
