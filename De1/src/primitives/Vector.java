package primitives;

public class Vector {

	private Point3D _head;

	// ***************** Constructors ********************** //
	/**
	 * build vector from three numbers
	 * 
	 * @param x the x value
	 * @param y the y value
	 * @param z the z value
	 */
	public Vector(double x, double y, double z) {
		_head = new Point3D(x, y, z);
		if (Point3D.ZERO.equals(_head))
			throw new IllegalArgumentException("Zero Vector is illegal");
	}

	/**
	 * build vector from three coordinates
	 * 
	 * @param x the x value
	 * @param y the y value
	 * @param z the z value
	 */
	public Vector(Coordinate x, Coordinate y, Coordinate z) {
		_head = new Point3D(x, y, z);
		if (Point3D.ZERO.equals(_head))
			throw new IllegalArgumentException("Zero Vector is illegal");
	}

	/**
	 * build vector from point3D
	 * 
	 * @param head the x,y,z values in point3D
	 */
	public Vector(Point3D head) {
		if (Point3D.ZERO.equals(head))
			throw new IllegalArgumentException("Zero Vector is illegal");
		_head = new Point3D(head);
	}

	/**
	 * Coordinate copy constructor
	 * 
	 * @param other Coordinate to copy from
	 */
	public Vector(Vector other) {
		_head = new Point3D(other._head);
	}

	// ***************** Getters/Setters ********************** //
	public Point3D getHead() {
		return _head;
	}

	// ***************** Administration ******************** //
	@Override
	/**
	 * the function checks if two vectors are equal
	 * @param obj
	 * @return true if equal false if not
	 */
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Vector))
			return false;
		Vector other = (Vector) obj;
		return other._head.equals(this._head);
	}

	/**
	 * the function return the components of the vectors
	 */
	public String toString() {
		return "[" + _head + "]";
	}

	// ***************** Operations ******************** //
	/**
	 * Subtract one vector from the other
	 * 
	 * @param other vector
	 * @return subtraction of two vectors
	 */
	public Vector sub(Vector other) {
		return _head.sub(other._head);
	}

	/**
	 * Calculate the sum of two vectors
	 * 
	 * @param other vector
	 * @return sum of two vectors
	 */
	public Vector add(Vector other) {
		return new Vector(_head.add(other));
	}

	/**
	 * Multiply vector and a number
	 * 
	 * @param number
	 * @return vector multiplied by a number
	 */
	public Vector scale(double num) {
		return new Vector(_head.scale(num));
	}

	/**
	 * Calculate the dot product of two vectors
	 * 
	 * @param other vector
	 * @return dot between two vectors
	 */
	public double dotProduct(Vector other) {
		return _head.getX().multiply(other._head.getX()) + _head.getY().multiply(other._head.getY())
				+ _head.getZ().multiply(other._head.getZ());
	}

	/**
	 * Calculate the cross product of two vectors
	 * 
	 * @param other vector
	 * @return cross product between two vectors
	 */
	public Vector crossProduct(Vector other) {
		return new Vector(_head.getY().multiply(other._head.getZ()) - _head.getZ().multiply(other._head.getY()),
				_head.getZ().multiply(other._head.getX()) - _head.getX().multiply(other._head.getZ()),
				_head.getX().multiply(other._head.getY()) - _head.getY().multiply(other._head.getX()));
	}

	/**
	 * 
	 * @return length of vector in the power of two
	 */
	public double length2() {
		return _head.distance2(Point3D.ZERO);
	}

	/**
	 * 
	 * @return length of vector
	 */
	public double length() {
		return _head.distance(Point3D.ZERO);
	}

	/**
	 * Creates new vector of the same direction and length 1
	 * 
	 * @return new normalized vector
	 */
	public Vector normalize() {
		return scale(1 / length());
	}
}
