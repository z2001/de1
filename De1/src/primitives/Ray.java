package primitives;

public class Ray {

	private Point3D _p00;
	private Vector _direction;

	// ***************** Constructors ********************** //
	/**
	 * build ray from point3D and a vector and normalize the vector
	 * 
	 * @param _p00 starting point of the ray
	 * @param _direction a normalized direction vector
	 */
	public Ray(Point3D _p00, Vector _direction) {
		this._p00 = new Point3D(_p00);
		this._direction = _direction.normalize();
	}

	/**
	 * Point3D and vector copy constructor
	 * 
	 * @param other ray to copy from
	 */
	public Ray(Ray other) {
		this._p00 = new Point3D(other._p00);
		this._direction = new Vector(other._direction);
	}

	// ***************** Getters/Setters ********************** //
	public Point3D getP00() {
		return _p00;
	}

	public Vector getDirection() {
		return _direction;
	}
	// ***************** Administration ******************** //

	/**
	 * the function checks two of point 2D and check if they equals
	 * 
	 * @param obj
	 * @return true if equal false if not
	 */
	public boolean equals(Ray obj) {
		return this._p00.equals(obj._p00) && this._direction.equals(obj._direction);
	}

	/**
	 * the function return the components of the ray
	 */
	public String toString() {
		return _p00 + "->" + _direction;
	}
}
